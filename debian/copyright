Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: foo-yc20
Upstream-Contact: Sampo Savolainen <v2@iki.fi>
Source: https://code.google.com/p/foo-yc20/

Files: *
Copyright: 2009-2010 Sampo Savolainen <v2@iki.fi>
License: GPL-3+

Files: include/lv2/*
Copyright:
 2008-2009 David Robillard <https://drobilla.net>
 2006-2007 Lars Luthman <lars.luthman@gmail.com>
License: LGPL-2+

Files: include/lv2/http/lv2plug.in/ns/extensions/ui/ui.h
Copyright:
 2008-2010 David Robillard <d@drobilla.net>
 2006 Steve Harris, David Robillard
 2000-2002 Richard W.E. Furse, Paul Barton-Davis
 2006-2008 Lars Luthman <lars.luthman@gmail.com>
License: LGPL-2.1+

Files: debian/*
Copyright:
 2010 Adrian Knoth <adi@drcomp.erfurt.thur.de>
 2010 Alessandro Ghedini <ghedo@debian.org>
 2011-2012 Alessio Treglia <alessio@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.